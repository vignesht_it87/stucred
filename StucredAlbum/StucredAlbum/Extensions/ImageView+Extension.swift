//
//  ImageView+Extension.swift
//  MusicAlbum
//
//  Created by Vignesh on 04/04/19.
//

import UIKit


import UIKit

class CacheImageModel: NSObject {
    var imageUrl:String?
    var downloadTask:URLSessionDownloadTask?
}

protocol CachedImageDelegate: class {
    func reloadImage()
}

class CacheImageView: UIImageView {
    weak var cacheDelegate:CachedImageDelegate?
    var imageModel:CacheImageModel = CacheImageModel()
    func loadImageWithURL(urlString:String)  {
        self.imageModel.imageUrl = urlString
        if DataManager.shared.imageCache != nil
        {
            if let imageObject:UIImage = DataManager.shared.imageCache.object(forKey: urlString as AnyObject) as? UIImage
            {
                self.image = imageObject
            } else
            {
                downloadImage(from: urlString)
            }
        } else
        {
            downloadImage(from: urlString)
        }
    }
    
    func downloadImage(from URL: String) {
        DataManager.shared.downloadImageFromURL(imageURL: URL) { (downloadedImage, error) in
            guard error != nil else {
                DispatchQueue.main.async {
                    DataManager.shared.imageCache.setObject(downloadedImage!, forKey: URL as AnyObject)
                    self.image = downloadedImage
                }
                return
            }
        }
    }
    
    func stopLoading() {
        if imageModel.downloadTask != nil
        {
            if imageModel.downloadTask!.state ==  URLSessionTask.State.running
            {
                imageModel.downloadTask!.cancel()
            }
        }
    }
    
}

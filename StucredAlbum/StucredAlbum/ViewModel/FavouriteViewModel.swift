//
//  FavouriteViewModel.swift
//  MusicAlbum
//
//  Created by Vignesh on 04/04/19.
//

import UIKit

struct FavouriteViewModel {
    private var favouriteModel: FavouritesModel
    
    var favouriteID: Int { return favouriteModel.favouriteId }
    
    var favouriteImage: UIImage { return favouriteModel.favouriteImage }
    
    var favouriteTitle: String { return favouriteModel.songTitle }
    
    init(favouriteData: FavouritesModel) {
        self.favouriteModel = favouriteData
    }
}

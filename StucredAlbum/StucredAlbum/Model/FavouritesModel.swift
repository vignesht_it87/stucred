//
//  FavouritesModel.swift
//  MusicAlbum
//
//  Created by Vignesh on 04/04/19.
//

import UIKit

struct FavouritesModel {
    var favouriteId: Int
    var favouriteImage: UIImage
    var songTitle: String
    
    init(id: Int, image: UIImage, title: String) {
        self.favouriteId = id
        self.favouriteImage = image
        self.songTitle = title
    }
}

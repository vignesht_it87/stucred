//
//  FavouritesCell.swift
//  MusicAlbum
//
//  Created by Vignesh on 04/04/19.
//

import UIKit

class FavouritesCell: UICollectionViewCell {
    private var favouriteViewModel: FavouriteViewModel!
    
    @IBOutlet weak var favouriteImageView: UIImageView!
    @IBOutlet weak var favouriteTitle: UILabel!
    
    override func layoutSubviews() {
        self.favouriteImageView.layer.cornerRadius = 10.0
    }
    
    func configureFavouriteCell(with Data: FavouriteViewModel) {
        self.favouriteViewModel = Data
        self.favouriteTitle.text = favouriteViewModel.favouriteTitle
        self.favouriteImageView.image = favouriteViewModel.favouriteImage
        self.layoutIfNeeded()
    }
}

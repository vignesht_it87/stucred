//
//  AlbumCell.swift
//  MusicAlbum
//
//  Created by Vignesh on 04/04/19.
//

import UIKit

class AlbumCell: UITableViewCell {
    private var albumData: AlbumViewModel!
    
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var songTitleLabel: UILabel!
    @IBOutlet weak var singerName: UILabel!
    @IBOutlet weak var cacheImageView: CacheImageView!
    
    override func layoutSubviews() {
        self.cacheImageView.layer.cornerRadius = 10.0
    }
    
    func setAlbum(with albumData: AlbumViewModel) {
        self.albumData = albumData
        cacheImageView.cacheDelegate = self as? CachedImageDelegate
        configureAlbumCellUI()
    }
    
    private func configureAlbumCellUI() {
        self.songTitleLabel.text = albumData.songTitle
        self.singerName.text = albumData.singerName
        self.cacheImageView.image = UIImage(named: "no-image-available")
        self.cacheImageView.stopLoading()
        self.cacheImageView.loadImageWithURL(urlString: albumData.songThumbnailImageURL)
    }
}

extension CacheImageView: CachedImageDelegate {
    func reloadImage() {
        self.reloadInputViews()
    }
}

//
//  AlbumViewController.swift
//  MusicAlbum
//
//  Created by Vignesh on 04/04/19.
//

import UIKit


class AlbumViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var favouritesView: UIView!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    
    var tableViewData = [AlbumViewModel]()
    var favouritesData = [FavouriteViewModel]()
    var filteredAlbums = [AlbumViewModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchAlbumDataAndReload()
        configureUI()
        // Do any additional setup after loading the view.
    }
    
    private func configureUI() {
        favouritesView.layer.borderColor = UIColor.gray.cgColor
        favouritesView.layer.borderWidth = 2.0
        favouritesView.layer.cornerRadius = 10.0
    }

    private func fetchAlbumDataAndReload() {
        DataManager.shared.fetchAlbumData { (albumData, error) in
            guard error != nil else {
                DispatchQueue.main.async {
                    self.tableViewData = albumData as! [AlbumViewModel]
                    self.tableView.reloadData()
                }
                return
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AlbumViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !(searchBar.text?.isEmpty)! {
            return filteredAlbums.count
        }
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "albumCell", for: indexPath) as! AlbumCell
        var albumViewModel = tableViewData[indexPath.row]
        if !(searchBar.text?.isEmpty)! && filteredAlbums.count > 0 {
            albumViewModel = filteredAlbums[indexPath.row]
        }
        cell.setAlbum(with: albumViewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var albumData = tableViewData[indexPath.row]
        if !(searchBar.text?.isEmpty)! && filteredAlbums.count > 0 {
            albumData = filteredAlbums[indexPath.row]
        }
        var isFavourite: Bool = false
        for data in favouritesData {
            if data.favouriteID == albumData.albumId {
                isFavourite = true
                break
            }
        }
        if !isFavourite {
            let favouriteViewModel = FavouriteViewModel(favouriteData: FavouritesModel(id: albumData.albumId, image: (DataManager.shared.imageCache.object(forKey: (albumData.songThumbnailImageURL as AnyObject)) as? UIImage ?? UIImage(named: "no-image-available")!), title: albumData.songTitle))
            favouritesData.append(favouriteViewModel)
            tableViewTopConstraint.constant = 105.0
            collectionView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
}


extension AlbumViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favouritesData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let favouriteCell = collectionView.dequeueReusableCell(withReuseIdentifier: "favouritesCell", for: indexPath) as! FavouritesCell
        let favouriteViewModel = favouritesData[indexPath.row]
        favouriteCell.configureFavouriteCell(with: favouriteViewModel)
        return favouriteCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var rowID: Int?
        var selectedData = tableViewData
        if !(searchBar.text?.isEmpty)! {
            selectedData = filteredAlbums
        }
        for data in selectedData {
            if favouritesData[indexPath.row].favouriteID == data.albumId {
                rowID = data.albumId
                break
            }
        }
        if rowID != nil {
            let indexPath = IndexPath(row: rowID!, section: 0)
            tableView.scrollToRow(at: indexPath, at: .none, animated: true)
        }
    }
}

extension AlbumViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContentForSearchText(searchText)
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredAlbums = tableViewData.filter({ (albumModel: AlbumViewModel) -> Bool in
            return albumModel.singerName.lowercased().contains(searchText.lowercased())  || albumModel.songTitle.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }
}
